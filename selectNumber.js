var compartment = [1,2,3,4,5,6,7,8,9];
var my_number = Math.floor(Math.random() * compartment.length);
var theNumberDrawn = compartment[my_number];
var input = $("input");
var information = $('#feedback');
var playAgain = $("#playAgain");

input.on('change', function () {

    var givenNumber= input.val();

    if (theNumberDrawn== givenNumber ){
        information.html("Wylosowałeś szukaną liczbe");
        information.attr('readonly',true);
        playAgain.removeClass('d-none');
    }else if( theNumberDrawn >givenNumber){
        information.html("Wylosowana liczba jest za mała");
    }else if ( theNumberDrawn < givenNumber && givenNumber < 10){
        information.html("Wylosowana liczba jest za duża");
    }else {
        information.html("Podaj liczbę z przedziału 1-9");
    }
});